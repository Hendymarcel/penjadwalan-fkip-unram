<?php
session_start();
require('db.php');

if (isset($_POST["import"])) {
    $fileName = $_FILES["file"]["tmp_name"];
    if ($_FILES["file"]["size"] > 0) {
        $jumlah_gagal = 0;
        $file = fopen($fileName, "r");
        $headers = fgetcsv($file, 10000, ",");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $kd_hari = "";
            if (isset($column[0])) {
              $kd_hari = mysqli_real_escape_string($con, $column[0]);
            }
            $jam_mulai = "";
            if (isset($column[1])) {
              $jam_mulai = mysqli_real_escape_string($con, $column[1]);
            }
            $jam_akhir = "";
            if (isset($column[2])) {
              $jam_akhir = mysqli_real_escape_string($con, $column[2]);
            }
            $jam_pelajaran = "";
            for($i=$jam_mulai;$i<=$jam_akhir;$i++) {
              $jam_pelajaran = $jam_pelajaran.sprintf("%'02d", $i).",";
            }
            $jam_pelajaran = substr($jam_pelajaran, 0, -1);
            $mata_kuliah = "";
            if (isset($column[3])) {
              $mata_kuliah = mysqli_real_escape_string($con, $column[3]);
            }
            $kelas = "";
            if (isset($column[4])) {
              $kelas = mysqli_real_escape_string($con, $column[4]);
            }
            $semester = "";
            if (isset($column[5])) {
              $semester = mysqli_real_escape_string($con, $column[5]);
            }
            $prodi = "";
            if (isset($column[6])) {
                $prodi = mysqli_real_escape_string($con, $column[6]);
            }
            $kd_dosen1 = "";
            if (isset($column[7])) {
                $kd_dosen1 = mysqli_real_escape_string($con, $column[7]);
            }
            $kd_dosen2 = "";
            if (isset($column[8])) {
                $kd_dosen2 = mysqli_real_escape_string($con, $column[8]);
                if($kd_dosen2=="") {
                  $kd_dosen2 = "NULL";
                } else {
                  $kd_dosen2 = "'".$kd_dosen2."'";
                }
            }
            $ruang = "";
            if (isset($column[9])) {
                $ruang = mysqli_real_escape_string($con, $column[9]);
            }
            $ins_query = "INSERT INTO jadwal(kd_hari,jam_pelajaran,mata_kuliah,kelas,semester,prodi,kd_dosen1,kd_dosen2,ruang) VALUES('".$kd_hari."','".$jam_pelajaran."','".$mata_kuliah."','".$kelas."','".$semester."','".$prodi."','".$kd_dosen1."',".$kd_dosen2.",'".$ruang."')";
            
            $jamRegex = str_replace(',', '|', $jam_pelajaran);
            $check_query = "SELECT * from jadwal WHERE (kd_hari='".$kd_hari."') AND (jam_pelajaran REGEXP '".$jamRegex."') AND (ruang='".$ruang."');";
            $result = $con->query($check_query);
            $row = mysqli_fetch_assoc($result);
            if(!empty($row)) {
              $jumlah_gagal++;
            } else {
              $check_query = "SELECT * from jadwal WHERE (kd_hari='".$kd_hari."') AND (jam_pelajaran REGEXP '".$jamRegex."') AND (kd_dosen1='".$kd_dosen1."' OR "."kd_dosen2='".$kd_dosen1."' OR kd_dosen1=".$kd_dosen2." OR "."kd_dosen2=".$kd_dosen2.");";
              $result = $con->query($check_query);
              $row = mysqli_fetch_assoc($result);
              if(!empty($row)) {
                $jumlah_gagal++;
              } else {
                $result = $con->query($ins_query);
                if(!$result) $jumlah_gagal++;
              }
            }
        }
        if($jumlah_gagal == 0) {
          $type = "success";
          $message = "Berhasil import data CSV ke database";
        } else {
          $type = "error";
          $message = "Gagal import ".$jumlah_gagal." data ke database";
        }
    }
}
?>
<!doctype html>
<html lang="en">
  <head>
  	<title>Penjadwalan/Import Jadwal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/style.css">
    
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
	  		<div class="img bg-wrap text-center py-4">
	  			<div class="user-logo">
	  				<div class="img" style="background-image: url(images/logo_unram.png);"></div>
	  			</div>
	  		</div>
        <ul class="list-unstyled components mb-5">
          <li>
            <a href="index.php"><span class="fa fa-user-circle-o mr-3"></span> Cek Jadwal Dosen</a>
          </li>
          <li>
              <a href="cariJadwal.php"><span class="fa fa-search mr-3 notif"></span> Cari Jadwal</a>
          </li>
          <?php
          if(isset($_SESSION["user"])) {
            echo '
              <li>
                <a href="manageJadwal.php"><span class="fa fa-cog mr-3"></span> Manage Jadwal</a>
              </li>
              <li class="active">
                <a href="importJadwal.php"><span class="fa fa-download mr-3"></span> Import Jadwal</a>
              </li>
              <li>
                <a href="exportJadwal.php"><span class="fa fa-print mr-3"></span> Export Jadwal</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
              </li>
            ';
          }
          else {
            echo '
              <li>
                <a href="signIn.php"><span class="fa fa-sign-in mr-3"></span> Sign In</a>
              </li>
            ';
          }
          ?>
        </ul>

    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <h2 class="mb-4">Import Jadwal</h2>
        <div id="response"
          class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
          <?php if(!empty($message)) { echo $message; } ?>
        </div>
        <div class="outer-scontainer">
          <div class="row">
            <form class="form-horizontal" action="" method="post" name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
              <div class="input-row">
                <label class="col-md-4 control-label">Upload File CSV</label>
                <input type="file" name="file" id="file" accept=".csv">
                <button type="submit" id="submit" name="import" class="btn-success">Import to Database</button>
                <br />
              </div>
            </form>
          </div>
        </div>
      </div>
		</div>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#frmCSVImport").on("submit", function () {
          $("#response").attr("class", "");
          $("#response").html("");
          var fileType = ".csv";
          var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
          if (!regex.test($("#file").val().toLowerCase())) {
            $("#response").addClass("error");
            $("#response").addClass("display-block");
            $("#response").html("Masukkan file berbentuk <b>" + fileType + "</b>");
            return false;
          }
          return true;
        });
      });
    </script>
    
    <div class="modal" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Sign Out <i class="fa fa-lock"></i></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <p><i class="fa fa-question-circle"></i> Apakah anda yakin Ingin keluar? <br /></p>
            <div class="actionsBtns">
              <form id="signout_form" action="signOut.php" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input onclick="signout_submit()" type="submit" class="btn btn-default btn-primary" data-dismiss="modal" value="Sign Out" />
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function signout_submit() {
      document.getElementById("signout_form").submit();
    }    
    </script>
  </body>
</html>