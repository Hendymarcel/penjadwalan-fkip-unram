<?php
session_start();
?>
<!doctype html>
<html lang="en">
  <head>
  	<title>Penjadwalan/Import Jadwal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/style.css">
    
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
	  		<div class="img bg-wrap text-center py-4">
	  			<div class="user-logo">
	  				<div class="img" style="background-image: url(images/logo_unram.png);"></div>
	  			</div>
	  		</div>
        <ul class="list-unstyled components mb-5">
          <li>
            <a href="index.php"><span class="fa fa-user-circle-o mr-3"></span> Cek Jadwal Dosen</a>
          </li>
          <li>
              <a href="cariJadwal.php"><span class="fa fa-search mr-3 notif"></span> Cari Jadwal</a>
          </li>
          <?php
          if(isset($_SESSION["user"])) {
            echo '
              <li>
                <a href="manageJadwal.php"><span class="fa fa-cog mr-3"></span> Manage Jadwal</a>
              </li>
              <li>
                <a href="importJadwal.php"><span class="fa fa-download mr-3"></span> Import Jadwal</a>
              </li>
              <li class="active">
                <a href="exportJadwal.php"><span class="fa fa-print mr-3"></span> Export Jadwal</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
              </li>
            ';
          }
          else {
            echo '
              <li>
                <a href="signIn.php"><span class="fa fa-sign-in mr-3"></span> Sign In</a>
              </li>
            ';
          }
          ?>
        </ul>

    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <h2 class="mb-4">Export Jadwal</h2>
        <div id="response"
          class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
          <?php if(!empty($message)) { echo $message; } ?>
        </div>
        <div class="outer-scontainer">
          <div class="row">
            <form action="export.php" method="get">
              <div class="input-group">
                <select name="prodi" id="prodi" class="custom-select border" required>
                  <option value="" selected>Prodi...</option>
                  <option value="Pendidikan Biologi"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Biologi')?' selected':''?>>Pendidikan Biologi</option>
                  <option value="Pendidikan Kimia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Kimia')?' selected':''?>>Pendidikan Kimia</option>
                  <option value="Pendidikan Fisika"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Fisika')?' selected':''?>>Pendidikan Fisika</option>
                  <option value="Pendidikan Matematika"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Matematika')?' selected':''?>>Pendidikan Matematika</option>
                  <option value="Pendidikan Pancasila dan Kewarganegaraan"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Pancasila dan Kewarganegaraan')?' selected':''?>>Pendidikan Pancasila dan Kewarganegaraan</option>
                  <option value="Pendidikan Sosiologi"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Sosiologi')?' selected':''?>>Pendidikan Sosiologi</option>
                  <option value="Pendidikan Bahasa dan Sastra Indonesia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Bahasa dan Sastra Indonesia')?' selected':''?>>Pendidikan Bahasa dan Sastra Indonesia</option>
                  <option value="Pendidikan Bahasa Inggris"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Bahasa Inggris')?' selected':''?>>Pendidikan Bahasa Inggris</option>
                  <option value="Pendidikan Guru SD (PGSD)"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Guru SD (PGSD)')?' selected':''?>>Pendidikan Guru SD (PGSD)</option>
                  <option value="Pendidikan Guru PAUD (PG.PAUD)"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Guru PAUD (PG.PAUD)')?' selected':''?>>Pendidikan Guru PAUD (PG.PAUD)</option>
                  <option value="Magister Pendidikan Bahasa Indonesia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Magister Pendidikan Bahasa Indonesia')?' selected':''?>>Magister Pendidikan Bahasa Indonesia</option>
                  <option value="Magister Pendidikan Bahasa Inggris"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Magister Pendidikan Bahasa Inggris')?' selected':''?>>Magister Pendidikan Bahasa Inggris</option>
                </select>
              </div>
              <div>
                <button type="submit" name="import" class="btn-success">Export to Excel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
		</div>
    
    <div class="modal" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Sign Out <i class="fa fa-lock"></i></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <p><i class="fa fa-question-circle"></i> Apakah anda yakin Ingin keluar? <br /></p>
            <div class="actionsBtns">
              <form id="signout_form" action="signOut.php" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input onclick="signout_submit()" type="submit" class="btn btn-default btn-primary" data-dismiss="modal" value="Sign Out" />
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function signout_submit() {
      document.getElementById("signout_form").submit();
    }
    </script>
  </body>
</html>