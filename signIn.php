<?php
    session_start();
    $message="";
    if(count($_POST)>0) {
        $con = mysqli_connect('localhost','root','','penjadwalan') or die('Unable To connect');
        $result = mysqli_query($con,"SELECT * FROM user WHERE username='" . $_POST["user_name"] . "' and password = '". $_POST["password"]."'");
        $row  = mysqli_fetch_array($result);
        if(is_array($row)) {
        $_SESSION["user"] = $row['username'];
        } else {
         $message = "Invalid Username or Password!";
        }
    }
    if(isset($_SESSION["user"])) {
    header("Location:index.php");
    }
?>

<html lang="en">
  <head>
  	<title>Penjadwalan/Sign In</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
	  		<div class="img bg-wrap text-center py-4">
	  			<div class="user-logo">
	  				<div class="img" style="background-image: url(images/logo_unram.png);"></div>
	  			</div>
	  		</div>
        <ul class="list-unstyled components mb-5">
          <li>
            <a href="index.php"><span class="fa fa-user-circle-o mr-3"></span> Cek Jadwal Dosen</a>
          </li>
          <li>
              <a href="cariJadwal.php"><span class="fa fa-search mr-3 notif"></span> Cari Jadwal</a>
          </li>
          <?php
          if(isset($_SESSION["user"])) {
            echo '
              <li>
                <a href="manageJadwal.php"><span class="fa fa-cog mr-3"></span> Manage Jadwal</a>
              </li>
              <li>
                <a href="importJadwal.php"><span class="fa fa-download mr-3"></span> Import Jadwal</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
              </li>
            ';
          }
          else {
            echo '
              <li class="active">
                <a href="#"><span class="fa fa-sign-in mr-3"></span> Sign In</a>
              </li>
            ';
          }
          ?>
        </ul>
    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <div class="container">
          <div class="row">
            <div class="col"></div>
            <div class="card col-6">
            <article class="card-body">
              <h4 class="card-title text-center mb-4 mt-1">Admin Sign in</h4>
              <hr>
              <p class="text-success text-center"><?php if($message!="") { echo $message; } ?></p>
              <form method="post" action="">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                   </div>
                  <input name="user_name" class="form-control border" placeholder="Username" type="username">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                   </div>
                    <input name="password" class="form-control border" placeholder="******" type="password">
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Sign In  </button>
              </div>
              </form>
            </article>
            </div>
            <div class="col"></div>
          </div>
        </div>
      </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>