<?php
session_start();
require('db.php');

$id=$_REQUEST['id'];
$status = "";
if(count($_REQUEST) > 1) {
  $id = $_REQUEST['id'];
	$hari = $_REQUEST['hari'];
  $jam_pelajaran = $_REQUEST['jam_pelajaran'];
  $mata_kuliah = $_REQUEST['mata_kuliah'];
  $kelas = $_REQUEST['kelas'];
  $semester = $_REQUEST['semester'];
  $prodi = $_REQUEST['prodi'];
  $kd_dosen1 = $_REQUEST['kd_dosen1'];
	if($_REQUEST['kd_dosen2'] == "")
		$kd_dosen2 = "NULL";
	else
		$kd_dosen2 = "'".$_REQUEST['kd_dosen2']."'";
  $ruang = $_REQUEST['ruang'];
  $update = "UPDATE jadwal SET kd_hari='".$hari."', jam_pelajaran='".$jam_pelajaran."', mata_kuliah='".$mata_kuliah."', kelas='".$kelas."', semester='".$semester."', prodi='".$prodi."', kd_dosen1='".$kd_dosen1."', kd_dosen2=".$kd_dosen2.", ruang='".$ruang."' where id_jadwal='".$id."'";
  
  $check_query = "SELECT * from jadwal WHERE (kd_hari='".$hari."') AND (jam_pelajaran REGEXP '[".$jam_pelajaran."]') AND (ruang='".$ruang."');";
  $result = $con->query($check_query);
  $row = mysqli_fetch_assoc($result);
  if(!empty($row) && $id!=$row["id_jadwal"]) {
    $status = "Tabrakan waktu pada ruang";
  } else {
    $check_query = "SELECT * from jadwal WHERE (kd_hari='".$hari."') AND (jam_pelajaran REGEXP '[".$jam_pelajaran."]') AND (kd_dosen1='".$kd_dosen1."' OR "."kd_dosen2='".$kd_dosen1."' OR kd_dosen1=".$kd_dosen2." OR "."kd_dosen2=".$kd_dosen2.");";
    $result = $con->query($check_query);
    $row = mysqli_fetch_assoc($result);
    if(!empty($row) && $id!=$row["id_jadwal"]) {
      $status = "Tabrakan waktu pada dosen";
    } else {
      $result = $con->query($update);
      if($result) $status = "Jadwal berhasil diedit.";
      else $status = "Jadwal gagal diedit.";
    }
  }
  
}

$query = "SELECT * from jadwal where id_jadwal='".$id."';";
$result = mysqli_query($con, $query) or die ( mysqli_error());
$row = mysqli_fetch_assoc($result);
$queryH = "Select * from hari where kd_hari = (Select kd_hari from jadwal where id_jadwal='".$id."');";
$result2 = mysqli_query($con,$queryH) or die ( mysqli_error());
$row2 = mysqli_fetch_assoc($result2);

?>
<!doctype html>
<html lang="en">
  <head>
  	<title>Edit Jadwal Jadwal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
	  		<div class="img bg-wrap text-center py-4">
	  			<div class="user-logo">
	  				<div class="img" style="background-image: url(images/logo_unram.png);"></div>
	  			</div>
	  		</div>
        <ul class="list-unstyled components mb-5">
          <li>
            <a href="#"><span class="fa fa-user-circle-o mr-3"></span> Cek Jadwal Dosen</a>
          </li>
          <li>
              <a href="cariJadwal.php"><span class="fa fa-search mr-3 notif"></span> Cari Jadwal</a>
          </li>
          <?php
          if(isset($_SESSION["user"])) {
            echo '
              <li>
                <a href="manageJadwal.php"><span class="fa fa-cog mr-3"></span> Manage Jadwal</a>
              </li>
              <li>
                <a href="importJadwal.php"><span class="fa fa-download mr-3"></span> Import Jadwal</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
              </li>
            ';
          }
          else {
            echo '
              <li>
                <a href="signIn.php"><span class="fa fa-sign-in mr-3"></span> Sign In</a>
              </li>
            ';
          }
          ?>
        </ul>

    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <h2 class="mb-4">Tambah Jadwal Baru</h2>
        <form name="form" method="post" action="">
          <input type="hidden" name="new" value="1" />
          <input name="id_jadwal" type="hidden" value="<?php echo $row['id_jadwal'];?>" />
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="hari">Hari</label>
              <select name="hari" class="form-control border" required>
                <option selected>Hari...</option>
                <option value="1"<?php echo ($row2['kd_hari']==1)?' selected':''?>>Senin</option>
                <option value="2"<?php echo ($row2['kd_hari']==2)?' selected':''?>>Selasa</option>
                <option value="3"<?php echo ($row2['kd_hari']==3)?' selected':''?>>Rabu</option>
                <option value="4"<?php echo ($row2['kd_hari']==4)?' selected':''?>>Kamis</option>
                <option value="5"<?php echo ($row2['kd_hari']==5)?' selected':''?>>Jumat</option>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label for="jam_pelajaran">Jam Pelajaran</label>
              <input type="text" class="form-control border" name="jam_pelajaran" value="<?php echo $row['jam_pelajaran'];?>">
            </div>
          </div>
          <div class="form-group">
            <label for="mata_kuliah">Mata Kuliah</label>
            <input type="text" class="form-control border" name="mata_kuliah" required value="<?php echo $row['mata_kuliah'];?>">
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="kelas">Kelas</label>
              <select name="kelas" class="form-control border">
                <option selected>Kelas...</option>
                <?php
                foreach(array("A", "B", "C", "D", "E", "F", "G") as $kelas) {
                  echo "<option value='".$kelas."'";
                  echo ($row['kelas']==$kelas)?' selected':'';
                  echo ">".$kelas."</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="semester">Semester</label>
              <select name="semester" class="form-control border">
                <option selected>Semester...</option>
                <?php
                for($i=1;$i<=8;$i++) {
                  echo "<option value='".$i."'";
                  echo ($row['semester']==$i)?' selected':'';
                  echo ">".$i."</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="prodi">Program Studi</label>
              <select name="prodi" class="form-control border">
                <option selected>Program Studi...</option>
                <option value="Pendidikan Biologi"<?php echo ($row['prodi']=="Pendidikan Biologi")?' selected':''?>>Pendidikan Biologi</option>
                <option value="Pendidikan Kimia"<?php echo ($row['prodi']=="Pendidikan Kimia")?' selected':''?>>Pendidikan Kimia</option>
                <option value="Pendidikan Fisika"<?php echo ($row['prodi']=="Pendidikan Fisika")?' selected':''?>>Pendidikan Fisika</option>
                <option value="Pendidikan Matematika"<?php echo ($row['prodi']=="Pendidikan Matematika")?' selected':''?>>Pendidikan Matematika</option>
                <option value="Pendidikan Pancasila dan Kewarganegaraan"<?php echo ($row['prodi']=="Pendidikan Pancasila dan Kewarganegaraan")?' selected':''?>>Pendidikan Pancasila dan Kewarganegaraan</option>
                <option value="Pendidikan Sosiologi"<?php echo ($row['prodi']=="Pendidikan Sosiologi")?' selected':''?>>Pendidikan Sosiologi</option>
                <option value="Pendidikan Bahasa dan Sastra Indonesia"<?php echo ($row['prodi']=="Pendidikan Bahasa dan Sastra Indonesia")?' selected':''?>>Pendidikan Bahasa dan Sastra Indonesia</option>
                <option value="Pendidikan Bahasa Inggris"<?php echo ($row['prodi']=="Pendidikan Bahasa Inggris")?' selected':''?>>Pendidikan Bahasa Inggris</option>
                <option value="Pendidikan Guru SD (PGSD)"<?php echo ($row['prodi']=="Pendidikan Guru SD (PGSD)")?' selected':''?>>Pendidikan Guru SD (PGSD)</option>
                <option value="Pendidikan Guru PAUD (PG.PAUD)"<?php echo ($row['prodi']=="Pendidikan Guru PAUD (PG.PAUD)")?' selected':''?>>Pendidikan Guru PAUD (PG.PAUD)</option>
                <option value="Magister Pendidikan Bahasa Indonesia"<?php echo ($row['prodi']=="Magister Pendidikan Bahasa Indonesia")?' selected':''?>>Magister Pendidikan Bahasa Indonesia</option>
                <option value="Magister Pendidikan Bahasa Inggris"<?php echo ($row['prodi']=="Magister Pendidikan Bahasa Inggris")?' selected':''?>>Magister Pendidikan Bahasa Inggris</option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="kd_dosen1">Kode Dosen 1</label>
              <input type="text" class="form-control border" name="kd_dosen1" required value="<?php echo $row['kd_dosen1'];?>">
            </div>
            <div class="form-group col-md-6">
              <label for="kd_dosen2">Kode Dosen 2</label>
              <input type="text" class="form-control border" name="kd_dosen2" value="<?php echo $row['kd_dosen2'];?>">
            </div>
          </div>
          <div class="form-group">
            <label for="ruang">Ruang</label>
            <input type="text" class="form-control border" name="ruang" required value="<?php echo $row['ruang'];?>">
          </div>
          <p class="text-success text-center"><?php if($status!="") { echo $status; } ?></p>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
		</div>
    
    <div class="modal" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Sign Out <i class="fa fa-lock"></i></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <p><i class="fa fa-question-circle"></i> Apakah anda yakin Ingin keluar? <br /></p>
            <div class="actionsBtns">
              <form id="signout_form" action="signOut.php" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input onclick="signout_submit()" type="submit" class="btn btn-default btn-primary" data-dismiss="modal" value="Sign Out" />
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function signout_submit() {
      document.getElementById("signout_form").submit();
    }    
    </script>
    
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
