<?php
session_start();
require('db.php');
?>

<!doctype html>
<html lang="en">
  <head>
  	<title>Penjadwalan/Manage Jadwal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	        </button>
        </div>
	  		<div class="img bg-wrap text-center py-4">
	  			<div class="user-logo">
	  				<div class="img" style="background-image: url(images/logo_unram.png);"></div>
	  			</div>
	  		</div>
        <ul class="list-unstyled components mb-5">
          <li>
            <a href="index.php"><span class="fa fa-user-circle-o mr-3"></span> Cek Jadwal Dosen</a>
          </li>
          <li>
              <a href="cariJadwal.php"><span class="fa fa-search mr-3 notif"></span> Cari Jadwal</a>
          </li>
          <?php
          if(isset($_SESSION["user"])) {
            echo '
              <li class="active">
                <a href="manageJadwal.php"><span class="fa fa-cog mr-3"></span> Manage Jadwal</a>
              </li>
              <li>
                <a href="importJadwal.php"><span class="fa fa-download mr-3"></span> Import Jadwal</a>
              </li>
              <li>
                <a href="exportJadwal.php"><span class="fa fa-print mr-3"></span> Export Jadwal</a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
              </li>
            ';
          }
          else {
            echo '
              <li>
                <a href="signIn.php"><span class="fa fa-sign-in mr-3"></span> Sign In</a>
              </li>
            ';
          }
          ?>
        </ul>

    	</nav>

        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <h2 class="mb-4">Manage Jadwal</h2>
        <?php
        include("manageJadwalSearch.php");
        ?>
      </div>
		</div>
    
    <div class="modal" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Sign Out <i class="fa fa-lock"></i></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <p><i class="fa fa-question-circle"></i> Apakah anda yakin Ingin keluar? <br /></p>
            <div class="actionsBtns">
              <form id="signout_form" action="signOut.php" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input onclick="signout_submit()" type="submit" class="btn btn-default btn-primary" data-dismiss="modal" value="Sign Out" />
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function signout_submit() {
      document.getElementById("signout_form").submit();
    }    
    </script>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>