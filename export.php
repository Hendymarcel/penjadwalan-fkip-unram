<?php
$filename = "Jadwal Prodi ".$_REQUEST["prodi"].".xls";
header('Content-Disposition: attachment; filename='.$filename );
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
?>
<html>
<head>
</head>
<body>
  <?php
  require('db.php');
  $nama_hari = array("", "Senin", "Selasa", "Rabu", "Kamis", "Jumat");
  $waktu = array(
		array(   #Bahasa indonesia, Bahasa inggris, Kimia, Biologi dan Matematika
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40"),
		  array("17:40","18:10"),
		  array("18:10","19:00")
		),
		array(   #PAUD dan PGSD hari selain jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		),
		array(   #PAUD dan PGSD hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("09:30","10:20"),
		  array("10:20","11:10"),
		  array("14:00","14:50"),
		  array("14:50","15:40"),
		  array("15:40","16:20"),
		  array("16:20","17:10")
		),
		array(   #FISIKA, PKN dan SOSIOLOGI hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		)
  );
  
  $query = "SELECT DISTINCT ruang FROM jadwal WHERE prodi='".$_REQUEST["prodi"]."' ORDER BY prodi ASC;";
  $result = $con->query($query);
  while($row = mysqli_fetch_assoc($result) ) {
  ?>
  <table border="1">
    <thead>
      <tr>
        <th scope="col">Hari</th>
        <th scope="col">Jam</th>
        <th scope="col">Waktu</th>
        <th scope="col">Ruang <?php echo $row["ruang"]; ?></th>
        <th scope="col">Smt</th>
        <th scope="col">Dosen</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $ruang = $row["ruang"];
      $query = "SELECT jam_pelajaran FROM jadwal WHERE prodi='".$_REQUEST["prodi"]."';";
      $result2 = $con->query($query);
      $jam_tertinggi = 1;
        while($row2 = mysqli_fetch_assoc($result2)) {
          $jam_baru = (int)substr($row2["jam_pelajaran"], -2);
          if($jam_tertinggi < $jam_baru) $jam_tertinggi = $jam_baru;
        }
      for($hari = 1; $hari <= 5; $hari++) {
        
        $versi = 0;
        if(in_array($_REQUEST["prodi"], array("Pendidikan Guru PAUD (PG.PAUD)", "Pendidikan Guru SD (PGSD)")) && $hari != 5) $versi = 1;
        else if(in_array($_REQUEST["prodi"], array("Pendidikan Guru PAUD (PG.PAUD)", "Pendidikan Guru SD (PGSD)")) && $hari == 5) $versi = 2;
        else if(in_array($_REQUEST["prodi"], array("Pendidikan Fisika", "Pendidikan Pancasila dan Kewarganegaraan", "Pendidikan Sosiologi")) && $hari == 5) $versi = 3;
        
        for($jam_pelajaran = 1; $jam_pelajaran <= $jam_tertinggi; $jam_pelajaran++) {
          $query = "SELECT * FROM jadwal WHERE  prodi='".$_REQUEST["prodi"]."' AND ruang='".$ruang."' AND kd_hari=".$hari." AND jam_pelajaran LIKE '%".sprintf("%'02d", $jam_pelajaran)."%';";
          $result2 = $con->query($query);
          if(mysqli_num_rows($result2)==0) {
      ?>
      <tr>
        <td><?php echo $nama_hari[$hari]; ?></td>
        <td><?php echo $jam_pelajaran ?></td>
        <td><?php echo $waktu[$versi][$jam_pelajaran][0]." - ".$waktu[$versi][$jam_pelajaran][1] ?></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <?php
        } else {
        $row2 = mysqli_fetch_assoc($result2);
      ?>
      <tr>
        <td><?php echo $nama_hari[$hari]; ?></td>
        <td><?php echo $jam_pelajaran ?></td>
        <td><?php echo $waktu[$versi][$jam_pelajaran][0]." - ".$waktu[$versi][$jam_pelajaran][1] ?></td>
        <td><?php echo $row2["mata_kuliah"]; ?></td>
        <td><?php echo $row2["semester"].$row2["kelas"]; ?></td>
        <td><?php echo $row2["kd_dosen1"]." ".$row2["kd_dosen2"]; ?></td>
      </tr>
      <?php }}} ?>
    </tbody>
  </table>
  <table>
  </table>
  <?php } ?>
</body>
</html>