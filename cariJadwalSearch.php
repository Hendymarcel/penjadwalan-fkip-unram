<?php
require('db.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Cari Jadwal Search</title>
</head>

<body>
  <div class="col-sm-12">
    <form method="get">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label>Mata Kuliah</label>
            <input type="text" name="mata_kuliah" id="mata_kuliah" class="form-control border" value="<?php echo isset($_REQUEST['mata_kuliah'])?$_REQUEST['mata_kuliah']:''?>" placeholder="Kimia Organik 2">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label>Kode Dosen</label>
            <input type="text" name="kd_dosen" id="kd_dosen" class="form-control border" value="<?php echo isset($_REQUEST['kd_dosen'])?$_REQUEST['kd_dosen']:''?>" placeholder="MUN">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label>Ruang</label>
            <input type="text" name="ruang" id="ruang" class="form-control border" value="<?php echo isset($_REQUEST['ruang'])?$_REQUEST['ruang']:''?>" placeholder="GKB 508">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Hari</label>
            <div class="btn-group-toggle d-flex justify-content-between" data-toggle="buttons">
              <label class="btn btn-secondary<?php echo (isset($_REQUEST['hari'])&&in_array(1,$_REQUEST['hari']))?' active':''?> col-2">
                <input type="checkbox" name="hari[]" value=1<?php echo (isset($_REQUEST['hari'])&&in_array(1,$_REQUEST['hari']))?' checked':''?> autocomplete="off"> Senin
              </label>
              <label class="btn btn-secondary<?php echo (isset($_REQUEST['hari'])&&in_array(2,$_REQUEST['hari']))?' active':''?> col-2">
                <input type="checkbox" name="hari[]" value=2<?php echo (isset($_REQUEST['hari'])&&in_array(2,$_REQUEST['hari']))?' checked':''?> autocomplete="off"> Selasa
              </label>
              <label class="btn btn-secondary<?php echo (isset($_REQUEST['hari'])&&in_array(3,$_REQUEST['hari']))?' active':''?> col-2">
                <input type="checkbox" name="hari[]" value=3<?php echo (isset($_REQUEST['hari'])&&in_array(3,$_REQUEST['hari']))?' checked':''?> autocomplete="off"> Rabu
              </label>
              <label class="btn btn-secondary<?php echo (isset($_REQUEST['hari'])&&in_array(4,$_REQUEST['hari']))?' active':''?> col-2">
                <input type="checkbox" name="hari[]" value=4<?php echo (isset($_REQUEST['hari'])&&in_array(4,$_REQUEST['hari']))?' checked':''?> autocomplete="off"> Kamis
              </label>
              <label class="btn btn-secondary<?php echo (isset($_REQUEST['hari'])&&in_array(5,$_REQUEST['hari']))?' active':''?> col-2">
                <input type="checkbox" name="hari[]" value=5<?php echo (isset($_REQUEST['hari'])&&in_array(5,$_REQUEST['hari']))?' checked':''?> autocomplete="off"> Jumat
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <label>Jam Pelajaran</label>
          <div class="input-group">
            <select name="jam_pelajaran" id="jam_pelajaran" class="custom-select border">
              <option value="" selected>Jam Pelajaran...</option>
              <?php
              for($i=1;$i<=15;$i++) {
                echo "<option value='".sprintf("%'02d", $i)."'";
                echo (isset($_REQUEST['jam_pelajaran'])&&$_REQUEST['jam_pelajaran']==$i)?' selected':'';
                echo ">".$i."</option>";
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <label>Kelas</label>
          <div class="input-group">
            <select name="kelas" id="kelas" class="custom-select border">
              <option value="" selected>Kelas...</option>
              <?php
              foreach(array("A", "B", "C", "D", "E", "F", "G") as $kelas) {
                echo "<option value='".$kelas."'";
                echo (isset($_REQUEST['kelas'])&&$_REQUEST['kelas']==$kelas)?' selected':'';
                echo ">".$kelas."</option>";
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <label>Semester</label>
          <div class="input-group">
            <select name="semester" id="semester" class="custom-select border">
              <option value="" selected>Semester...</option>
              <?php
              for($i=1;$i<=8;$i++) {
                echo "<option value='".$i."'";
                echo (isset($_REQUEST['semester'])&&$_REQUEST['semester']==$i)?' selected':'';
                echo ">".$i."</option>";
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <label>Prodi</label>
          <div class="input-group">
            <select name="prodi" id="prodi" class="custom-select border">
              <option value="" selected>Prodi...</option>
              <option value="Pendidikan Biologi"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Biologi')?' selected':''?>>Pendidikan Biologi</option>
              <option value="Pendidikan Kimia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Kimia')?' selected':''?>>Pendidikan Kimia</option>
              <option value="Pendidikan Fisika"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Fisika')?' selected':''?>>Pendidikan Fisika</option>
              <option value="Pendidikan Matematika"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Matematika')?' selected':''?>>Pendidikan Matematika</option>
              <option value="Pendidikan Pancasila dan Kewarganegaraan"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Pancasila dan Kewarganegaraan')?' selected':''?>>Pendidikan Pancasila dan Kewarganegaraan</option>
              <option value="Pendidikan Sosiologi"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Sosiologi')?' selected':''?>>Pendidikan Sosiologi</option>
              <option value="Pendidikan Bahasa dan Sastra Indonesia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Bahasa dan Sastra Indonesia')?' selected':''?>>Pendidikan Bahasa dan Sastra Indonesia</option>
              <option value="Pendidikan Bahasa Inggris"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Bahasa Inggris')?' selected':''?>>Pendidikan Bahasa Inggris</option>
              <option value="Pendidikan Guru SD (PGSD)"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Guru SD (PGSD)')?' selected':''?>>Pendidikan Guru SD (PGSD)</option>
              <option value="Pendidikan Guru PAUD (PG.PAUD)"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Pendidikan Guru PAUD (PG.PAUD)')?' selected':''?>>Pendidikan Guru PAUD (PG.PAUD)</option>
              <option value="Magister Pendidikan Bahasa Indonesia"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Magister Pendidikan Bahasa Indonesia')?' selected':''?>>Magister Pendidikan Bahasa Indonesia</option>
              <option value="Magister Pendidikan Bahasa Inggris"<?php echo (isset($_REQUEST['prodi'])&&$_REQUEST['prodi']=='Magister Pendidikan Bahasa Inggris')?' selected':''?>>Magister Pendidikan Bahasa Inggris</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label> </label>
            <div>
              <button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Search</button>
              <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i class="fa fa-fw fa-trash"></i> Clear</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="form">
    <table class="table table-striped table-hover table-bordered table-responsive-sm">
      <thead class="bg-success">
        <tr>
          <th scope="col">Hari</th>
          <th scope="col">Jam mulai</th>
          <th scope="col">Jam akhir</th>
          <th scope="col">Mata kuliah</th>
          <th scope="col">Kls</th>
          <th scope="col">Smt</th>
          <th scope="col">Prodi</th>
          <th scope="col">Dosen 1</th>
          <th scope="col">Dosen 2</th>
          <th scope="col">Ruang</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $filter = "WHERE TRUE ";
	  
        if(isset($_REQUEST['mata_kuliah']) && $_REQUEST['mata_kuliah'] != '')
          $filter = $filter."AND (mata_kuliah LIKE '%".$_REQUEST["mata_kuliah"]."%') ";
        if(isset($_REQUEST['kd_dosen']) && $_REQUEST['kd_dosen'] != '')
          $filter = $filter."AND (kd_dosen1='".$_REQUEST["kd_dosen"]."' OR "."kd_dosen2='".$_REQUEST["kd_dosen"]."') ";
        if(isset($_REQUEST['ruang']) && $_REQUEST['ruang'] != '')
          $filter = $filter."AND (ruang='".$_REQUEST["ruang"]."') ";
        if(isset($_REQUEST['kelas']) && $_REQUEST['kelas'] != '')
          $filter = $filter."AND (kelas='".$_REQUEST["kelas"]."') ";
        if(isset($_REQUEST['semester']) && $_REQUEST['semester'] != '')
          $filter = $filter."AND (semester='".$_REQUEST["semester"]."') ";
        if(isset($_REQUEST['prodi']) && $_REQUEST['prodi'] != '')
          $filter = $filter."AND (prodi='".$_REQUEST["prodi"]."') ";
        
        if(isset($_REQUEST['hari']) and !empty($_REQUEST['hari'])) {
          $filter = $filter."AND (";
          foreach($_REQUEST['hari'] as $h) {
            $filter = $filter."(jadwal.kd_hari='".$h."') OR ";
          }
          $filter = substr($filter, 0, -3);
          $filter = $filter.") ";
        }
		
        if(isset($_REQUEST['jam_pelajaran']) && $_REQUEST['jam_pelajaran'] != '')
        $filter = $filter."AND (jam_pelajaran LIKE '%".$_REQUEST['jam_pelajaran']."%') ";
		
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $results_per_page;
        
        $query = "Select * from hari inner join jadwal on hari.kd_hari = jadwal.kd_hari ".$filter." ORDER BY hari.kd_hari asc LIMIT $start_from, ".$results_per_page.";";
        $result = $con->query($query);
        
		$jam = array(
		array(						#Bahasa indonesia, Bahasa inggris, Kimia, Biologi dan Matematika
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40"),
		  array("17:40","18:10"),
		  array("18:10","19:00")
		),
		array( 						#PAUD dan PGSD hari selain jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		),
		array( 						#PAUD dan PGSD hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("09:30","10:20"),
		  array("10:20","11:10"),
		  array("14:00","14:50"),
		  array("14:50","15:40"),
		  array("15:40","16:20"),
		  array("16:20","17:10")
		),
		array( 						#FISIKA, PKN dan SOSIOLOGI hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		));
		
        while($row = mysqli_fetch_assoc($result) ) { 
		if('$row["mata_kuliah"] != null'){
		  $versi = 0;
		
		  if(($row["prodi"] == "Pendidikan PAUD" || $row["prodi"] == "Pendidikan PGSD") && ($row["nama_hari"] != "Jumat")){
			$versi = 1;
		  }
		  else if(($row["prodi"] == "Pendidikan PAUD" || $row["prodi"] == "Pendidikan PGSD") && ($row["nama_hari"] == "Jumat")){
			$versi = 2;
		  }
		  else if(($row["prodi"] == "Pendidikan Fisika" || $row["prodi"] == "Pendidikan PKN" || $row["prodi"] == "Pendidikan Sosiologi") && ($row["nama_hari"] == "Jumat")){
			$versi = 3;
		  }
		 
		  $jam_mulai = $jam[$versi][(int)substr($row["jam_pelajaran"], 0, 2)-1][0];
		  $jam_akhir = $jam[$versi][(int)substr($row["jam_pelajaran"], -2)-1][1];
		
		?>
        <tr>
          <td align="center"><?php echo $row["nama_hari"];?></td>
          <td align="center"><?php echo date("H:i", strtotime("$jam_mulai"));?></td>
          <td align="center"><?php echo date("H:i", strtotime("$jam_akhir"));?></td>
          <td align="center"><?php echo $row["mata_kuliah"];?></td>
          <td align="center"><?php echo $row["kelas"];?></td>
          <td align="center"><?php echo $row["semester"];?></td>
          <td align="center"><?php echo $row["prodi"];?></td>
          <td align="center"><?php echo $row["kd_dosen1"];?></td>
          <td align="center"><?php echo $row["kd_dosen2"];?></td>
          <td align="center"><?php echo $row["ruang"];?></td>
        </tr>
        <?php }}?>
      </tbody>
    </table>
  </div>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <?php
      $sql = "SELECT COUNT(id_jadwal) AS total FROM jadwal ".$filter.";"; 
      $result2 = $con->query($sql);
      $row = $result2->fetch_assoc();
      $total_pages = ceil($row["total"] / $results_per_page);
      $url=$_SERVER["QUERY_STRING"];
      if(substr($url, 0, 4)=="page") {
        $url = substr($url, strpos($url, "&")+1);
      }
      
      for ($i=1; $i<=$total_pages; $i++) {
        echo "
          <li class='page-item'>
            <a class='page-link' href='".$_SERVER["PHP_SELF"]."?page=".$i."&".$url."' if ($i==$page);>$i</a>
          </li>
        ";
      }
      ?>
    </ul>
  </nav>
</body>
</html>
