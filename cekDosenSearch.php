<?php
require('db.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Cek Dosen Search</title>
</head>

<body>
  <div class="col-sm-12">
    <form method="get">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label>Kode Dosen</label>
            <input type="text" name="kd_dosen" id="kd_dosen" class="form-control border" value="<?php echo isset($_REQUEST['kd_dosen'])?$_REQUEST['kd_dosen']:''?>" placeholder="MUN">
          </div>
        </div>
        <div class="col-sm-8"></div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label> </label>
            <div>
              <button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Search</button>
              <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i class="fa fa-fw fa-sync"></i> Clear</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="form">
    <table class="table table-striped table-hover table-bordered table-responsive-sm">
      <thead class="bg-success">
        <tr>
          <th scope="col">Hari</th>
          <th scope="col">Jam mulai</th>
          <th scope="col">Jam akhir</th>
          <th scope="col">Mata kuliah</th>
          <th scope="col">Kls</th>
          <th scope="col">Smt</th>
          <th scope="col">Prodi</th>
          <th scope="col">Dosen 1</th>
          <th scope="col">Dosen 2</th>
          <th scope="col">Ruang</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $filter = "WHERE TRUE ";
        
        if(isset($_REQUEST['kd_dosen']) && $_REQUEST['kd_dosen'] != '')
          $filter = $filter."AND (kd_dosen1='".$_REQUEST["kd_dosen"]."' OR "."kd_dosen2='".$_REQUEST["kd_dosen"]."') ";
        
		if(isset($_REQUEST['jam_pelajaran']) && $_REQUEST['jam_pelajaran'] != '')
          $filter = $filter."AND (jam_pelajaran LIKE '%".$_REQUEST['jam_pelajaran']."%') ";
		
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $results_per_page;
        
        $query = "Select * from hari inner join jadwal on hari.kd_hari = jadwal.kd_hari ".$filter." ORDER BY hari.kd_hari asc LIMIT $start_from, ".$results_per_page.";";
        $result = $con->query($query);
        
		$jam = array(
		array(						#Bahasa indonesia, Bahasa inggris, Kimia, Biologi dan Matematika
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40"),
		  array("17:40","18:10"),
		  array("18:10","19:00")
		),
		array( 						#PAUD dan PGSD hari selain jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		),
		array( 						#PAUD dan PGSD hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("09:30","10:20"),
		  array("10:20","11:10"),
		  array("14:00","14:50"),
		  array("14:50","15:40"),
		  array("15:40","16:20"),
		  array("16:20","17:10")
		),
		array( 						#FISIKA, PKN dan SOSIOLOGI hari jumat
		  array("07:00","07:50"),
		  array("07:50","08:40"),
		  array("08:40","09:30"),
		  array("10:20","11:10"),
		  array("11:10","12:00"),
		  array("13:30","14:20"),
		  array("14:20","15:10"),
		  array("15:10","16:00"),
		  array("16:00","16:50"),
		  array("16:50","17:40")
		));
		
        while($row = mysqli_fetch_assoc($result) ) { 
		if('$row["mata_kuliah"] != null'){
		  $versi = 0;
		
		  if(($row["prodi"] == "Pendidikan PAUD" || $row["prodi"] == "Pendidikan PGSD") && ($row["nama_hari"] != "Jumat")){
			$versi = 1;
		  }
		  else if(($row["prodi"] == "Pendidikan PAUD" || $row["prodi"] == "Pendidikan PGSD") && ($row["nama_hari"] == "Jumat")){
			$versi = 2;
		  }
		  else if(($row["prodi"] == "Pendidikan Fisika" || $row["prodi"] == "Pendidikan PKN" || $row["prodi"] == "Pendidikan Sosiologi") && ($row["nama_hari"] == "Jumat")){
			$versi = 3;
		  }
		 
		  $jam_mulai = $jam[$versi][(int)substr($row["jam_pelajaran"], 0, 2)-1][0];
		  $jam_akhir = $jam[$versi][(int)substr($row["jam_pelajaran"], -2)-1][1];
			
        ?>
        <tr>
          <td align="center"><?php echo $row["nama_hari"];?></td>
          <td align="center"><?php echo date("H:i", strtotime($jam_mulai));?></td>
          <td align="center"><?php echo date("H:i", strtotime($jam_akhir));?></td>
          <td align="center"><?php echo $row["mata_kuliah"];?></td>
          <td align="center"><?php echo $row["kelas"];?></td>
          <td align="center"><?php echo $row["semester"];?></td>
          <td align="center"><?php echo $row["prodi"];?></td>
          <td align="center"><?php echo $row["kd_dosen1"];?></td>
          <td align="center"><?php echo $row["kd_dosen2"];?></td>
          <td align="center"><?php echo $row["ruang"];?></td>
        </tr>
        <?php }}?>
      </tbody>
    </table>
  </div>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <?php
      $sql = "SELECT COUNT(id_jadwal) AS total FROM jadwal ".$filter.";"; 
      $result2 = $con->query($sql);
      $row = $result2->fetch_assoc();
      $total_pages = ceil($row["total"] / $results_per_page);
      $url=$_SERVER["QUERY_STRING"];
      if(substr($url, 0, 4)=="page") {
        $url = substr($url, strpos($url, "&")+1);
      }
      
      for ($i=1; $i<=$total_pages; $i++) {
        echo "
          <li class='page-item'>
            <a class='page-link' href='".$_SERVER["PHP_SELF"]."?page=".$i."&".$url."' if ($i==$page);>$i</a>
          </li>
        ";
      }
      ?>
    </ul>
  </nav>
</body>
</html>
